﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo_81
{
    class Program
    {
        static Random rnd = new Random();
        public static List<dynamic> MaListe<t>()
        {
            List<dynamic> Malist = new List<dynamic>();
            for (int x = 0; x < 10; ++x)
            {
                dynamic nombreall = rnd.Next(1, 50);
                Malist.Add(nombreall);
                Console.Write($" {nombreall} ");
            }
            return Malist;
        }

        public static void TrierListe<t>(ref List<t> ListeATrier)
        {
            bool Liste_En_Ordre = false;
            int taille = ListeATrier.Count;
            while(!Liste_En_Ordre)
            {
                Liste_En_Ordre = true;
                for (dynamic i = 0; i < taille - 1; ++i)
                {
                    if (ListeATrier[i] > ListeATrier[i+1])
                    {
                        dynamic a = ListeATrier[i];
                        ListeATrier[i] = ListeATrier[i + 1];
                        ListeATrier[i + 1] = a;
                        Liste_En_Ordre = false;
                    }
                }
                taille--;
            }
            AfficherListe(ListeATrier);
        }

        public static void AfficherListe<t>(List<t> SortedList)
        {
            Console.SetCursorPosition(0, 5);
            Console.WriteLine("Voici la liste trier");
            for (int x = 0; x < 10; ++x)   
                Console.Write($" {SortedList[x]} ");
        }

        static void Main(string[] args)
        {
           
            List<dynamic> Lit = MaListe<dynamic>();
             
            TrierListe(ref Lit);
            Console.SetCursorPosition(0, 10);
        }
    }
}
