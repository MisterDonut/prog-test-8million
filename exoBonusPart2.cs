﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace _12._9Bonus
{
    class Transaction
    {

        
        public double Cout;

        public double CalcPrix(int Photocopie)
        {
            for (int x = 1; x <= Photocopie; x++)
            {
                if (x <= 10)
                {
                    Cout += 0.10;
                }
                else if (x > 10 && x <= 20)
                {
                    Cout += 0.08;
                }
                else
                {
                    Cout += 0.07;
                }
               
            }
            return Cout;
        }
    }
}
