﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace l_exo_81__cour_8_
{
    class Program
    {
        static void Main(string[] args)
        {
            //-------------------------------------EXO-81------------------------------------\\
            Dictionary<char, List<char>> monGraphe = new Dictionary<char, List<char>>();
            monGraphe.Add('A', new List<Char>() { 'B', 'D' });
            monGraphe.Add('B', new List<Char>() { 'A', 'C' });
            monGraphe.Add('C', new List<Char>() { 'B', 'D','E' });
            monGraphe.Add('D', new List<Char>() { 'A', 'C' });
            monGraphe.Add('E', new List<Char>() { 'C'});

            //-------------------------------------EXO-82------------------------------------\\

            Console.WriteLine($@"            _______________________________________________________         ");
            Console.WriteLine($@"            \  __________________________________________________  |        ");
            Console.WriteLine($@"            / / Prog                                     Maxim.H | |     ");
            Console.WriteLine($@"            \ \ Exo 82                                  07/02/18 | |    ");
            Console.WriteLine($@"            / /                                                  | |");
            Console.WriteLine($@"            \ \ ()               _____________                   | |");
            Console.WriteLine($@"            / /                 |             |                  | |");
            Console.WriteLine($@"            \ \                 |    Karen    |                  | |");
            Console.WriteLine($@"            / /                 |_____________|                  | |");
            Console.WriteLine($@"            \ \                  /           \                   | | ");
            Console.WriteLine($@"            / /    _____________/             \_____________     | |");
            Console.WriteLine($@"            \ \   |             |             |             |    | |");
            Console.WriteLine($@"            / /   |   Maxime    |             |   Charles   |    | |");
            Console.WriteLine($@"            \ \   |_____________|             |_____________|    | |");
            Console.WriteLine($@"            / /             \                     /              | |");
            Console.WriteLine($@"            \ \ ()           \   _____________   /               | |");
            Console.WriteLine($@"            / /               \ |             | /                | |");
            Console.WriteLine($@"            \ \                \|     Bob     |/                 | |");
            Console.WriteLine($@"            / /                 |_____________|                  | |");
            Console.WriteLine($@"            \ \                        |                         | |");
            Console.WriteLine($@"            / /                  ______|______                   | |");
            Console.WriteLine($@"            \ \                 |             |                  | |");
            Console.WriteLine($@"            / /                 |  Alexandre  |                  | |");
            Console.WriteLine($@"            \ \                 |_____________|                  | |");
            Console.WriteLine($@"            / / ()                                               | |");
            Console.WriteLine($@"            \ \                                                  | |");
            Console.WriteLine($@"            / /                                                  | |  ");
            Console.WriteLine($@"            \ \__________________________________________________| |  ");
            Console.WriteLine($@"            /______________________________________________________|  ");


            //-------------------------------------EXO-83------------------------------------\\
            /*
             * Ordre DFS Graphe exo 81
             * {B,A,C,D} {B,C,E}
             */
            //-------------------------------------EXO-84------------------------------------\\
            /*
             * Ordre BFS Graphe exo 81
             * {B,C,E} {B,A,D}
             */
        }
    }
}
