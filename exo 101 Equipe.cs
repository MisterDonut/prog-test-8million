﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo_101.Structure
{
    public struct Équipe
    {
        public string Nom;
        public List<Joueur> LesJoueurs;
        public int Score;

        public equipe(string LeNom, List<Joueur> player, int point )
        {
            Nom = LeNom;
            LesJoueurs = player;
            Score = point;
        }
    }
}