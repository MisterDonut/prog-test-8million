﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo_cour_3
{
    class Program
    {
        static void Main(string[] args)
        {
            public static double sommeTableau(double[] monTableau)
            {
                double somme = 0;
                for (int i = 0; i < monTableau.Length; i++)
                {
                    somme += monTableau[i];
                }
                return somme;
            }

            // Je ne comprend pas quel est la longueur du tableau




            public static double sommeMatrice(double[,] maMatrice)
            {
                double somme = 0;
                for (int i = 0; i < maMatrice.GetLength(0); i++)//Parcours lignes
                {
                    for (int j = 0; j < maMatrice.GetLength(1); j++)//Parcours colonnes
                    {
                        somme += maMatrice[i, j];
                    }
                }
                return somme;
            }

            //      Etape 5 : Temps Constant O(1)

            public static int fonctionInutile(int n)
            {
                if (n <= 1)
                {
                    return 1;
                }
                else
                {
                    return fonctionInutile(n - 1) - fonctionInutile(n - 1);
                }
            }

            //     N = 0 : etape = 1
            //   N = 1 : etape = 1
            // N = 2 : etape = 2
            // N = 3 : etape = 3 
            // N = 4 : etape = 4

            //     O(n) : Lineaire

        }
    }
}
