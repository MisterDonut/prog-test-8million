﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exo_87
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<char, List<char>> monGraphe = new Dictionary<char, List<char>>();
            monGraphe.Add('A', new List<Char>() { 'D', 'E' });
            monGraphe.Add('B', new List<Char>() { 'D', 'E' });
            monGraphe.Add('C', new List<Char>() { 'D', 'E' });
            monGraphe.Add('D', new List<Char>() { 'A', 'B', 'C' });
            monGraphe.Add('E', new List<Char>() { 'A', 'B', 'C' });
        }
    }
}
