﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exo12._8
{
    class Program
    {

        static bool PresenceNombreImpair(List<int> MaList)
        {
            for (int x = 0; x < MaList.Count; x++)
            {
                for (int y = 1; y < 100; y = y + 2)
                {
                    if (MaList[x] == y)
                    {
                        return true;
                    }
                   
                }
            }
            return false;
        }
        

        static void Main(string[] args)
        {
            List<int> UneListe = new List<int>();
            UneListe.Add(2);
            UneListe.Add(4);
            UneListe.Add(6);
            UneListe.Add(8);
            UneListe.Add(66);
            UneListe.Add(33);
            
            Console.WriteLine(PresenceNombreImpair(UneListe));
            
        }
    }
}
