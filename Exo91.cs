﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace exo_91
{
    public struct Personnage
    {
        public string Nom;
        public string Race;
        public int PoinDeVie;
        public int damage;
        public int Defense;

        public Personnage(string Name, string Racial, int hp, int dmg, int def)
        {
            Nom = Name;
            Race = Racial;
            PoinDeVie = hp;
            damage = dmg;
            Defense = def;
        }

        public int PowerUP()
        {
            damage = damage * 2;

            return damage;
        }

        public bool AmIDead()
        {
            if (PoinDeVie < 1)
                return false;

            else
                return true;
        }

        public void AfficherPersonnage()
        {
            Console.WriteLine($"nom: {Nom}");
            Console.WriteLine($"race: {Race}");
            Console.WriteLine($"Pdv: {PoinDeVie}");
            Console.WriteLine($"dmg: {damage}");
            Console.WriteLine($"def: {Defense}");
        }

        public bool FrapperEnnemi(ref Personnage P2)
        {
            Console.WriteLine($"{Nom} attack {P2.Nom}");
           


            int DegatTotal = damage - P2.Defense;
            if (DegatTotal < 1)
                DegatTotal = 1;
          
            Console.WriteLine($"attack = {DegatTotal}");

            P2.PoinDeVie = P2.PoinDeVie - DegatTotal;

            Console.WriteLine($"{P2.Nom} : {P2.PoinDeVie} HP");
            Console.WriteLine();
            return P2.AmIDead();

        }
    }

    class Program
    {
        static Random rnd = new Random();

        static void Main(string[] args)
        {
            Personnage perso1 = new Personnage("nathan", "Human", 70, 4, 5 );
            Personnage perso2 = new Personnage("takaski", "elf", 40, 9, 2);

            Combat(ref perso1, ref perso2);
        }


        public static void Combat(ref Personnage perso1, ref Personnage perso2)
        {
            bool alive1 = true, alive2 = true;
            int x = rnd.Next(0, 2);
            int y = rnd.Next(0, 10);
            do
            {
                switch (x)
                {
                    case 0:   
                        if (y == 5)
                        {
                            perso1.PowerUP();
                        }
                        alive2 = perso1.FrapperEnnemi(ref perso2);
                        break;
                    case 1:
                        alive1 = perso2.FrapperEnnemi(ref perso1);
                        if (y == 9)
                        {
                            perso2.PowerUP();
                        }
                        break;
                }
                if (x == 0)
                    x = 1;
                else
                    x = 0;


                Thread.Sleep(500);
            } while (alive1 & alive2);

            if (alive1 == false)
            {
                Console.WriteLine($"{perso2.Nom} Won the combat");
            }
            else if (alive2 == false)
            {
                Console.WriteLine($"{perso1.Nom} Won the combat");
            }
        }
    }
}