﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exo12._6
{
    class Program
    {

        public static T CalculerSomme <T>(List<T> maListe)
        {
            dynamic total = 0;
            maListe.GetType();
            for (int x = 0; x < maListe.Count; x++)
            {
                total += maListe[x];
            }

            return total;
        }

        static void Main(string[] args)
        {
            List<int> Liste1 = new List<int> {1,2,3,4,5,6,7,8,9}; // = 45
            List<double> Liste2 = new List<double> { 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8 }; // = 10.5



            Console.WriteLine("Somme liste int : " + CalculerSomme(Liste1));
            Console.WriteLine("Somme liste double : " + CalculerSomme(Liste2));
        }
    }
}
